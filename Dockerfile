
FROM ghcr.io/googlecloudplatform/spark-operator:v1beta2-1.3.7-3.1.1
RUN curl https://archive.apache.org/dist/spark/spark-3.2.1/spark-3.2.1-bin-hadoop3.2.tgz --output spark.tgz
RUN tar -xvzf ./spark.tgz -C ./
RUN cp ./spark-3.2.1-bin-hadoop3.2/jars/* /opt/spark/jars
